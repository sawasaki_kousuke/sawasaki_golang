package main

import (
	"golang.org/x/tour/pic"
	"image"
	"image/color"
)

type Image struct {
	width  int
	height int
}

func (i Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, i.width, i.height)
}
func (i Image) ColorModel() color.Model {
	return color.RGBAModel
}
func (i Image) At(x, y int) color.Color {
	r, g, b := uint8(x), uint8(y), uint8(x*y)
	return color.RGBA{r, g, b, 255}
}

func main() {
	m := Image{256, 256}
	pic.ShowImage(m)
}
