package main

import (
	"fmt"
	"math"
)

const E = 2.71828182846

func main() {
	v, w, x, y := 42, "String", math.E, true
	fmt.Printf("type(v) is %T\n", v)
	fmt.Printf("type(w) is %T\n", w)
	fmt.Printf("type(x) is %T\n", x)
	fmt.Printf("type(y) is %T\n", y)
	fmt.Println(E)
}
