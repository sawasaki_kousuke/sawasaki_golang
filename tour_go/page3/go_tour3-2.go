package main

import "fmt"

type Vector struct {
	x int
	y int
}

func main() {
	var vector Vector = Vector{1, 5}
	fmt.Println(vector)
	vector.x, vector.y = 5, 8
	fmt.Println(vector)
	fmt.Println(vector.x, vector.y)

	var p *Vector = &vector
	fmt.Println(p)
	p.x, p.y = 10, 100
	fmt.Println(vector)

	var v1 Vector = Vector{}
	var v2 Vector = Vector{x: 2}
	var v3 Vector = Vector{y: 4}
	fmt.Println(v1, v2, v3)

}
