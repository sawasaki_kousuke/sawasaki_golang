package main

import "fmt"

func main() {
	board := [][]int{
		[]int{1, 2, 3},
		[]int{4, 5, 6},
		[]int{7, 8, 9},
		make([]int, 10),
	}
	fmt.Println(board)
	temp := []int{5, 25, 125, 625, 3125}
	after := append(board, temp)
	fmt.Println(after)

}
