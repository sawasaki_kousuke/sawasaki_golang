package main

import "fmt"

func f1(x int) { //値渡し
	x = 10 * x
}
func f2(x *int) { //参照渡し
	*x = 10 * (*x)
}
func main() {
	var i, j int = 42, 2701
	var pi, pj *int = &i, &j
	fmt.Println(*pi, pi)
	fmt.Println(*pj, pj)

	var v = 10
	fmt.Println("initial", v)
	f1(v)
	fmt.Println("after function1", v)
	f2(&v)
	fmt.Println("after function2", v)
}
