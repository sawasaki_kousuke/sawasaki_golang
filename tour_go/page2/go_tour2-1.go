package main

import (
	"fmt"
)

func sum1(start, end int) (sum int) {
	if start > end {
		start, end = end, start
	}
	for i := start; i <= end; i++ {
		sum += i
	}
	return
}

func sum2(initial int) (sum int) {
	sum = initial
	for sum <= 10000 {
		sum += sum
	}
	return
}

func factorial(n int) int {
	if n == 0 {
		return 1
	} else {
		return factorial(n-1) * n
	}
}

func limit(v, lim int) {
	if w := v * v; w < lim {
		fmt.Println("OK")
	} else {
		fmt.Println("NG")
	}
}

func main() {
	var start, end int = 1, 100
	fmt.Println(sum1(start, end))
	fmt.Println(sum2(start))
	fmt.Println(factorial(10))
	limit(1, 100)
	limit(100, 100)
}
