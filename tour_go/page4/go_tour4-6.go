package main

import "fmt"

func f(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Println("int")
	case string:
		fmt.Println("string")
	default:
		fmt.Println(v, "not default")
	}
}

func main() {
	var i interface{} = 5

	s, ok := i.(int)
	fmt.Println(s, ok)

	w, wk := i.(string)
	fmt.Println(w, wk)

	f(i)

	var j interface{} = "String"
	f(j)
	j = true
	f(j)
	j = 2.71828182846
	f(j)
}
