package main

import "fmt"

func main() {
	m := make(map[string]int)
	m["java"] = 5
	m["python3"] = 10
	fmt.Println(m)

	strings := []string{"C", "C++", "C++", "Java", "Python3", "Perl"}
	for _, key := range strings {
		m[key] += 1
	}
	fmt.Println(m)
}
