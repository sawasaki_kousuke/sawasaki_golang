package main

import (
	"fmt"
	"math"
)

type Vector struct {
	x, y float64
}

func (v Vector) Abs() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func (v *Vector) Scale(f float64) {
	v.x *= f
	v.y *= f
}

func Scale(v *Vector, f float64) {
	v.x *= f
	v.y *= f
}

func main() {
	v, w := Vector{3, 4}, Vector{5, 12}
	fmt.Println(v.Abs())
	fmt.Println(w.Abs())
	Scale(&v, 20)
	w.Scale(12)
	fmt.Println(v.Abs())
	fmt.Println(w.Abs())
}
