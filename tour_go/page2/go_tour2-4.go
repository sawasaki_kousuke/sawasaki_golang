package main

import (
	"fmt"
)

func fizz_buzz() {
	for i := 1; i <= 30; i++ {
		switch {
		case i%15 == 0:
			fmt.Println("FizzBuzz")
		case i%5 == 0:
			fmt.Println("Buzz")
		case i%3 == 0:
			fmt.Println("Fizz")
		default:
			fmt.Println(i)
		}

	}

}

func defer_fizz_buzz() {
	for i := 1; i <= 30; i++ {
		switch {
		case i%15 == 0:
			defer fmt.Println("FizzBuzz")
		case i%5 == 0:
			defer fmt.Println("Buzz")
		case i%3 == 0:
			defer fmt.Println("Fizz")
		default:
			defer fmt.Println(i)
		}

	}

}

func main() {
	fizz_buzz()
	fmt.Println("==============================")
	defer_fizz_buzz()
}
