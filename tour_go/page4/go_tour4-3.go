package main

import (
	"fmt"
	"math"
)

type Vector struct {
	x, y float64
}

func (v Vector) Abs() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func Abs(v Vector) float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func main() {
	v := Vector{3, 4}
	p := &v
	fmt.Println(v.Abs())
	fmt.Println(Abs(v))
	fmt.Println(p.Abs())
	fmt.Println(Abs(*p))
}
