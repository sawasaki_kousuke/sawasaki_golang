package main

import "fmt"

func fibonacci() func(int) int {
	var v1, v2 int = 0, 1
	return func(x int) int {
		if x == 0 {
			return v1
		} else if x == 1 {
			return v2
		} else {
			v1, v2 = v2, v1+v2
			return v2
		}
	}
}
func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f(i))
	}
}
