package main

import (
	"fmt"
	"math"
)

type Myfloat float64

type Vector struct {
	x, y float64
}

func (v Vector) Abs() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func (f Myfloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func main() {
	v, w := Vector{3, 4}, Vector{5, 12}
	var f Myfloat = -math.E
	fmt.Println(v.Abs())
	fmt.Println(w.Abs())
	fmt.Println(f.Abs())
}
