package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func sum(s []int, c chan int) {
	sum_value := 0
	for _, v := range s {
		sum_value += v
	}
	fmt.Println(sum_value)
	c <- sum_value
}

func main() {
	go say("world")
	go say("hello")
	say("Ogura Yui")

	c := make(chan int)
	s1 := []int{1, 2, 4, 8, 16, 32}
	s2 := make([]int, 1000000000)
	go sum(s1, c)
	go sum(s2, c)
	x, y := <-c, <-c

	fmt.Println(x, y, x+y)
}
