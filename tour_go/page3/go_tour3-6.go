package main

import (
	"fmt"
	"golang.org/x/tour/pic"
)

func P(dx, dy int) [][]uint8 {
	var slice [][]uint8
	for i := 0; i < dy; i++ {
		sub_slice := make([]uint8, dx)
		for j := 0; j < dx; j++ {
			sub_slice[j] = uint8(i * j)
		}
		slice = append(slice, sub_slice)
	}
	return slice
}

func main() {
	pic.Show(P)
}
