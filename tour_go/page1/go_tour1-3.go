package main

import (
	"fmt"
)

func add(x, y int) int {
	return x + y
}

func add_mul(x, y int) (int, int) {
	return x + y, x * y
}

func swap_string(x, y string) (string, string) {
	return y, x
}

func div_rest(x, y int) (div, rest int) {
	div = x / y
	rest = x % y
	return
}

func main() {
	var a, b int = 33, 12
	fmt.Println(add(a, b))
	fmt.Println(add_mul(a, b))
	var str1, str2 string = "test1", "test2"
	fmt.Println(swap_string(str1, str2))
	fmt.Println(div_rest(a, b))
}
