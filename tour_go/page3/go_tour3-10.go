package main

import "fmt"

func adder(initial int) func(int) int {
	var sum int = initial
	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {
	pos := adder(1000)
	for i := 0; i < 10; i++ {
		fmt.Println(pos(i))
	}
	pos = adder(0)
	for i := 0; i < 10; i++ {
		fmt.Println(pos(i))
	}
}
