package main

import "fmt"

func main() {
	vs := [10]int{1, 2, 4, 8, 16, 32, 64, 128, 256, 512}
	fmt.Println(vs)

	va := vs[:6]
	vb := vs[4:]
	fmt.Println(va, vb)

	va[5] = -1
	fmt.Println(va, vb)
	fmt.Println(vs)

	for i, v := range vs {
		fmt.Println("index:", i, "value:", v)
	}

}
