package main

import (
	"golang.org/x/tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	word_count := make(map[string]int)
	var words []string = strings.Fields(s)
	for _, word := range words {
		word_count[word] += 1
	}
	return word_count
}

func main() {
	wc.Test(WordCount)
}
