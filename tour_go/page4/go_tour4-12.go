package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (reader rot13Reader) Read(buffer []byte) (int, error) {
	n, err := reader.r.Read(buffer)
	if err != nil {
		return 0, err
	}
	for i := range buffer {
		buffer[i] = reader.Rot13(buffer[i])
	}
	return n, nil
}

func (reader rot13Reader) Rot13(c byte) byte {
	c += 13
	if (91 <= c && c <= 96) || 123 <= c {
		c -= 26
	}
	return c
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
