package main

import (
	"fmt"
	"math"
	"math/rand"
)

func main() {
	rand.Seed(1)
	fmt.Println("number is ", rand.Intn(10))
	rand.Seed(3)
	fmt.Println("number is ", rand.Intn(10))
	fmt.Println("pi and E:", math.Pi, math.E)
}
