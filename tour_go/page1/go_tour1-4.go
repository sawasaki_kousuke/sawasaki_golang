package main

import (
	"fmt"
	"math"
)

func main() {
	var c, python, php, java, lisp bool = true, true, false, true, false
	fmt.Println(c, python, php, java, lisp)

	var q, w, e, r = 500, "It is String.", math.E, true
	fmt.Println(q, w, e, r)

	t, y := 3+5, "charcter"
	fmt.Println(t, y)

	var i int
	var f float64
	var b bool
	var s string
	fmt.Println(i, f, b, s)

}
