package main

import "fmt"

func main() {
	vectors := []struct {
		x int
		y int
	}{
		{2, 3},
		{4, 5},
		{6, 7},
		{8, 9},
	}

	fmt.Println(vectors)
	vectors = vectors[:2]
	fmt.Println(len(vectors), cap(vectors))
	vectors = vectors[:1]
	fmt.Println(len(vectors), cap(vectors))

	zeros := make([]int, 20)
	fmt.Println(zeros)

}
