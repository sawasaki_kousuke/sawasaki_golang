package main

import (
	"fmt"
	"math"
)

func compute(fn func(float64, float64) int) int {
	return fn(5, 12)
}

func main() {
	hypot := func(x, y float64) int {
		return int(math.Sqrt(x*x + y*y))
	}
	fmt.Println(hypot(3, 4))
	fmt.Println(compute(hypot))
}
