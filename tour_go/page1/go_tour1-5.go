package main

import (
	"fmt"
	"math"
)

func main() {
	var v float64 = math.Pi
	fmt.Println(v)

	var w int = int(v)
	fmt.Println(w)

	var u uint = uint(w)
	fmt.Println(u)

}
