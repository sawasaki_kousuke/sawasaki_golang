package main

import (
	"fmt"
	"math"
)

type I interface {
	Abs() float64
	Set(float64, float64)
}
type V struct {
	x float64
	y float64
}

func (v *V) Abs() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}
func (v *V) Set(x, y float64) {
	v.x *= x
	v.y *= y
}

func main() {
	var v I = &V{3, 4}
	fmt.Println(v.Abs())
	v.Set(5, 12)
	fmt.Println(v.Abs())

	var w *V
	fmt.Println(w)
	w = &V{5, 12}
	fmt.Println(w)
	fmt.Println(w.Abs())
}
